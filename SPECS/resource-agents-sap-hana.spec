#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the SAPHanaSR upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="SAPHanaSR-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version e711383f

%global saphana_prefix SAPHanaSR
%global saphana_version 6281418

Name:		resource-agents-sap-hana
Summary:	SAP HANA cluster resource agents
Epoch:		1
Version:	0.162.1
Release:	2%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:	GPLv2+
URL:		https://github.com/SUSE/SAPHanaSR
%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
Group:		System Environment/Base
%else
Group:		Productivity/Clustering/HA
%endif
Source0:	%{upstream_prefix}-%{upstream_version}.tar.gz
Source1:	%{saphana_prefix}-%{saphana_version}.tar.gz

BuildArch:	noarch

BuildRequires: automake autoconf gcc
BuildRequires: perl-interpreter python3-devel
BuildRequires: libxslt glib2-devel

%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%endif

Requires:	resource-agents >= 4.1.1
Conflicts:	%{name}-scaleout

Requires:	/bin/bash /usr/bin/grep /bin/sed /bin/gawk
Requires:	perl

%description
The SAP HANA resource agents interface with Pacemaker to allow
SAP instances to be managed in a cluster environment.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%setup -q -T -D -a 1 -n %{upstream_prefix}-%{upstream_version}

# add SAPHana agents to Makefile.am
mv %{saphana_prefix}-%{saphana_version}/ra/SAPHana* heartbeat
sed -i -e '/			SAPInstance		\\/a\			SAPHana			\\\n			SAPHanaTopology		\\' heartbeat/Makefile.am
sed -i -e '/                          ocf_heartbeat_SAPInstance.7 \\/a\                          ocf_heartbeat_SAPHana.7 \\\n                          ocf_heartbeat_SAPHanaTopology.7 \\' doc/man/Makefile.am

# change provider company
sed -i -e 's/\("provider_company": \)"SUSE"/\1"Red Hat"/g' %{saphana_prefix}-%{saphana_version}/srHook/SAPHanaSR.py

#%patch0 -p1

%build
if [ ! -f configure ]; then
	./autogen.sh
fi

%global rasset linux-ha

%configure BASH_SHELL="/bin/bash" \
	PYTHON="%{__python3}" \
	%{conf_opt_fatal} \
	--with-pkg-name=%{name} \
	--with-ras-set=%{rasset}

%if %{defined jobs}
JFLAGS="$(echo '-j%{jobs}')"
%else
JFLAGS="$(echo '%{_smp_mflags}')"
%endif

make $JFLAGS

%install
make install DESTDIR=%{buildroot}

# remove other agents
find %{buildroot}/usr/lib/ocf ! -type d ! -iname "SAPHana*" -exec rm {} \;
find %{buildroot}/%{_mandir} -type f ! -iname "*SAPHana*" -exec rm {} \;

mkdir -p %{buildroot}/%{_datadir}/SAPHanaSR/srHook
cp -r %{saphana_prefix}-%{saphana_version}/srHook/global.ini %{buildroot}/%{_datadir}/SAPHanaSR/srHook
cp -r %{saphana_prefix}-%{saphana_version}/srHook/SAPHanaSR.py %{buildroot}/%{_datadir}/SAPHanaSR/srHook

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_usr}/lib/ocf/resource.d/heartbeat/SAPHana*
%{_mandir}/man7/*SAP*
%{_datadir}/SAPHanaSR

%exclude /etc
%exclude /usr/include
%exclude /usr/lib/debug
%exclude /usr/lib/systemd
%exclude /usr/lib/tmpfiles.d
%exclude /usr/libexec
%exclude /usr/sbin
%exclude /usr/share/%{name}
%exclude /usr/src

%changelog
* Mon Mar 20 2023 Janine Fuchs <jfuchs@redhat.com> - 0.162.1-2
- Rebase to SAPHanaSR 0.162.1 upstream release.

  Resolves: rhbz#2165668

* Thu Feb  3 2022 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.154.0-7
- SAPHana/SAPHanaTopology: follow OCF standard for version and OCF
  version in metadata

  Resolves: rhbz#2050205

* Tue Feb  1 2022 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.154.0-6
- SAPHana/SAPHanaTopology: add systemd support

  Resolves: rhbz#2026273

* Tue Nov 30 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.154.0-4
- SAPHana/SAPHanaTopology: remove HANA_CALL_TIMEOUT parameter from
  metadata

  Resolves: rhbz#2026727

* Thu Jul 22 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.154.0-3
- SAPHana: use actual_mode from global.ini and fallback to mode when
  it's not set

  Resolves: rhbz#1855885

* Fri Feb 14 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.154.0-2
- Rebase to SAPHanaSR 0.154.0 upstream release.

  Resolves: rhbz#1802104

* Fri Sep  6 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.152.22-1
- SAPHanaTopology: make multi instance aware (MCOS)

  Resolves: rhbz#1738205

* Tue Jul 30 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.152.21-4
- Initial build as separate package

  Resolves: rhbz#1688344
